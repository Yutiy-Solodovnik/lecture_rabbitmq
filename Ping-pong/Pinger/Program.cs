﻿using System;
using System.Threading;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper wrapper = new Wrapper("pongExchange", "ping_queue", "ping", "pingExchange", "pong_queue", "pong");
            wrapper.messageDispached += Ping;

            Console.WriteLine("===PINGER===");

            wrapper.SendMessageToQueue("ping");
            wrapper.ListenQueue(() => wrapper.SendMessageToQueue("ping"));
            Console.ReadKey();
            wrapper.Dispose();
        }
        static void Ping()
        {
            Console.Beep(800, 250);
        }
    }
}
