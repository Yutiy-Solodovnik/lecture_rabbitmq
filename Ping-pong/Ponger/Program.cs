﻿using System;
using System.Threading;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper wrapper = new Wrapper("pingExchange", "pong_queue", "pong", "pongExchange", "ping_queue", "ping");
            wrapper.messageDispached += Pong;

            Console.WriteLine("===PONGER===");

            wrapper.ListenQueue(() => wrapper.SendMessageToQueue("pong"));
            Console.ReadKey();
            wrapper.Dispose();
        }

        static void Pong()
        {
            Console.Beep(400, 250);
        }
    }
}
