﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MyConnectionFactory
    {
        public static ConnectionFactory CreateConection(Uri uri)
        {
            return new ConnectionFactory()
            {
                Uri = uri,
                RequestedConnectionTimeout = TimeSpan.FromSeconds(30),
                NetworkRecoveryInterval = TimeSpan.FromSeconds(30),
                AutomaticRecoveryEnabled = true,
                TopologyRecoveryEnabled = true,
                RequestedHeartbeat = TimeSpan.FromTicks(60)
            };
        }
    }
}
