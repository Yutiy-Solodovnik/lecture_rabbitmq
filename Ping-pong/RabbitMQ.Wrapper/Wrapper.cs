﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;

namespace RabbitMQ.Wrapper
{
    public class Wrapper : IDisposable
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly Uri uri = new Uri("amqp://guest:guest@localhost:5672");

        public delegate void MessageHandler();
        public event MessageHandler messageDispached;

        public Wrapper(string consumerExchange, string consumerQueue, string consumerRoutingKey, string producerExchange, string producerQueue, string producerRoutingKey)
        {
            _messageConsumerScope = new MessageConsumerScopeFactory(MyConnectionFactory.CreateConection(uri)).Connect(
                new MessageScopeSettings
                {
                    ExchangeName = consumerExchange,
                    ExhangeType = ExchangeType.Direct,
                    QueueName = consumerQueue,
                    RoutingKey = consumerRoutingKey
                });

            _messageProducerScope = new MessageProducerScopeFactory(MyConnectionFactory.CreateConection(uri)).Open(
                 new MessageScopeSettings
                 {
                     ExchangeName = producerExchange,
                     ExhangeType = ExchangeType.Direct,
                     QueueName = producerQueue,
                     RoutingKey = producerRoutingKey
                 });
        }

        public void SendMessageToQueue(string message)
        {
            Thread.Sleep(1250);
            _messageProducerScope.MessageProducer.Send(message);
            Console.WriteLine($"{message} [{DateTime.Now} - dispatched]");
            messageDispached?.Invoke();
        }

        public void ListenQueue(Action sendMessage)
        {
            _messageConsumerScope.MessageConsumer.Received += (mess, e) =>
            {
                string message = Encoding.UTF8.GetString(e.Body.ToArray());
                Console.WriteLine($"{message} [{DateTime.Now} - received]");
                _messageConsumerScope.MessageConsumer.SetAcknowledge(e.DeliveryTag, true);
                Thread.Sleep(1250);
                sendMessage.Invoke();
            };
        }

        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }
    }
}
