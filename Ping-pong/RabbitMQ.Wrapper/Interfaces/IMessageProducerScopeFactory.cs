﻿using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScopeFactory
    {
        public IMessageProducerScope Open(MessageScopeSettings messageScopeSettings);
    }
}
