﻿using System;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumer
    {
        public event EventHandler<BasicDeliverEventArgs> Received;
        public void Connect();
        public void SetAcknowledge(ulong deliveryTag, bool processed);
    }
}
