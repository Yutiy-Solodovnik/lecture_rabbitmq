﻿using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScopeFactory
    {
        public IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings);
        public IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings);
    }
}
