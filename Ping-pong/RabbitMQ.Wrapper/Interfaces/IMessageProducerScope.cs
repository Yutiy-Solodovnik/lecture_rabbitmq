﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScope
    {
        public IMessageProducer MessageProducer { get; }

        void Dispose();
    }
}
