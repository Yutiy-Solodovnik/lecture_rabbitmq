﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageQueue : IDisposable
    {
        public IModel Channel { get; }
        public void DeclareExchange(string exchangeName, string exchangeType);
        public void BindQueue(string exhangeName, string routingKey, string queueName);
    }
}
