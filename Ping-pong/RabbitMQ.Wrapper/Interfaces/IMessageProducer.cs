﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducer
    {
        public void Send(string message, string type = null);
        public void SendType(Type type, string message);
    }
}
