﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScope
    {
        public IMessageConsumer MessageConsumer { get; }

        void Dispose();
    }
}
