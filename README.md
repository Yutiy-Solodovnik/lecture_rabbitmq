# README #

#Ping-pong game, using RabbitMQ   
One console application listens to a queue called ping_queue, the second listens to a pong_queue queue   
If a new message appears on the queue, the listener should send the message to the opposite queue with a 2.5 second delay.   
For example:   
* New message arrives in ping_queue   
* Current date are printed time and the message to the console (in this case, pong - since the message came from the opposite application)   
* Delay 2.5 seconds and send a new message to the pong_queue with a ping message   
* And so on in a cycle    